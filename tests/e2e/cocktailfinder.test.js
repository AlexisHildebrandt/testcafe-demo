import SearchByName from "./page/SearchByName";
import SearchByIngredient from "./page/SearchByIngredient";
import CocktailsByIngredientResponseVerifier from './CocktailsByIngredientResponseVerifier'

const cocktailsByIngredientResponseVerifier = new CocktailsByIngredientResponseVerifier()

fixture `Cocktailfinder`
    .requestHooks(cocktailsByIngredientResponseVerifier.requestLogger)
    .page `http://localhost:4545/`

// eslint-disable-next-line no-unused-vars
test('Search by name', async t => {
    await SearchByName.clickMenuItem('by name')
    await SearchByName.verifyPageHeading('Find cocktails by name')
    /*
    TODO:
    For each task, there is already a stubbed method either in SearchByName or in BasePage


    1. Find your favorite cocktail
    2. click on the name
    3. verify the name in the dialog heading
    4. close the dialog
    5. click on one of the ingredients in the table
    6. verify the page by checking the headline



    To insert a break point, use
    await t.debug()
    You can then use chrome dev tools by pressing F12


    To insert a pause, use
    await t.wait(milliseconds)
     */


});

// eslint-disable-next-line no-unused-vars
test('Search by Ingredient', async t => {
    /*
    TODO:
    for this test, there are no stubs. Create your own methods on the page model as needed.

    1. click "by ingredient" in the main menu
    2. select an ingredient from the select box
        HINT: t.typeText(SelectorElement, value).pressKey('down enter')
    4. click on one of the cocktails
    5. verify the name in the dialog heading
    6. close the dialog

    Bonus task 1:
    Verify one of the cocktails contained in the list by using the response verifier
        await t.expect(cocktailsByIngredientResponseVerifier.verifyResponseContainsCocktail({
            strDrink: 'the name',
            strDrinkThumb: 'the url',
            idDrink: 'the id'
    })).ok()


    Bonus task 2:
    Create a new response verifier for the detail request, and verify the list of ingredients after opening the dialog.
     */
});
