import {RequestLogger} from "testcafe";
import zlib from "zlib";


export default class CocktailsByIngredientResponseVerifier {
    constructor() {
        this.requestLogger = RequestLogger(/https:\/\/www\.thecocktaildb\.com\/api\/json\/v1\/1\/filter\.php\?i=(.*)/g, {
            logResponseBody: true,
            logResponseHeaders: true,
        })
    }

    verifyResponseContainsCocktail({strDrink, strDrinkThumb, idDrink}) {
        return this.requestLogger.contains(record => {
            try {
                let responsebody = this.parseResponse(record.response);

                return responsebody.drinks.find(cocktail =>
                    cocktail.strDrink === strDrink &&
                    cocktail.strDrinkThumb === strDrinkThumb &&
                    cocktail.idDrink === idDrink
                )

            } catch (e) {
                console.error(e)
                return false
            }
        })
    }

    parseResponse(response) {
        let body
        let contentEncoding = response.headers['content-encoding'];
        if (contentEncoding === 'br') {
            body = zlib.brotliDecompressSync(response.body).toString('utf-8')
        } else if (contentEncoding === 'gzip') {
            body = zlib.gunzipSync(response.body).toString('utf-8')
        } else {
            body = response.body
        }

        return JSON.parse(body)
    }
}
