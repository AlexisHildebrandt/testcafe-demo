import {Selector, t} from 'testcafe'

const selector = {
    pageHeading: '.page-heading',
    menuItem: '.el-menu-item',
}

export default class BasePage {
    menuItem(text) {
        return Selector(selector.menuItem).withText(text)
    }

    async clickMenuItem(text) {
        let menuItem = this.menuItem(text);
        await t
            .click(menuItem)
            .expect(menuItem.hasClass('is-active')).ok()
    }

    pageHeading(text) {
        return Selector(selector.pageHeading).withText(text)
    }

    verifyPageHeading(expected) {
        let actual = this.pageHeading(expected).textContent
        return t.expect(actual).eql(expected, `headline should equal "${expected}"`, {timeout: 30000})
    }

    async closeDialog() {
        // TODO implement
    }
}
