import CocktailsByName from "@/components/CocktailsByName"
import CocktailsByIngredient from "@/components/CocktailsByIngredient"
import VueRouter from "vue-router"

const routes = [
    {path: '/search/name', component: CocktailsByName, name: 'search.name'},
    {path: '/search/ingredient', component: CocktailsByIngredient, name: 'search.ingredient'},
]

export default new VueRouter({routes})
