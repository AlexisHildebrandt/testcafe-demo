import axios from "axios"

export default {
    findCocktailsByName: async(search) => await axios.get(`https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${search}`),
    findCocktailsByIngredient: async(search) => await axios.get(`https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=${search}`),
    getCocktail: async(id) => await axios.get(`https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${id}`),
    getCategories: async() => await axios.get('https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list'),
    getIngredients: async() => await axios.get('https://www.thecocktaildb.com/api/json/v1/1/list.php?i=list'),
    getIngredient: async(id) => await axios.get(`https://www.thecocktaildb.com/api/json/v1/1/lookup.php?iid=${id}`),
}
