import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue'
import VueRouter from 'vue-router'
import router from '@/router'

import elementLangEn from 'element-ui/lib/locale/lang/en'
import elementLocale from 'element-ui/lib/locale'

elementLocale.use(elementLangEn)


Vue.use(ElementUI);
Vue.use(VueRouter)

Vue.config.productionTip = false



new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
