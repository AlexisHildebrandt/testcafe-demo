# testcafe-demo

## Project setup
```
yarn install
```

### Run the frontend
```
yarn serve
```

### Run the E2E-tests in chrome
```
yarn test:chrome
```

