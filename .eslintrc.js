module.exports = {
  root: true,

  env: {
    node: true,
    browser: true,
  },

  rules: {
    'vue/component-name-in-template-casing': ['error', 'kebab-case'],
    'vue/require-default-prop': 'off',
    'vue/max-attributes-per-line': 'off',
    'vue/singleline-html-element-content-newline': 'off',
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
  },

  "plugins": [
    "testcafe"
  ],

  parserOptions: {
    parser: 'babel-eslint',
  },

  extends: ['plugin:vue/recommended', 'eslint:recommended','plugin:vue/essential', 'plugin:testcafe/recommended'],

  overrides: [
    {
      files: ['**/__tests__/*.{j,t}s?(x)', '**/tests/unit/**/*.spec.{j,t}s?(x)'],
      env: {
        jest: true,
      },
    },
  ],
}
